from django.shortcuts import render , redirect
import urllib2
from models import  *
from forms import *
import requests
import BeautifulSoup
Soup = BeautifulSoup.BeautifulSoup
import crawl
import redis
import html5lib
from rq.decorators import job
import memcache
import re
from memcache import RedisCache

connection = redis.StrictRedis(host='pub-redis-10172.us-east-1-4.2.ec2.garantiadata.com', port=10172, db=0, password="jakieshaslo")
kesz = RedisCache(connection)

def index(request):
    return render(request, 'index.html')

def onet(request):

    context = None
    onet_cache = kesz.get('onet-cache')

    if onet_cache is None:
        Onetpl.objects.all().delete()
        url = "http://www.onet.pl"
        tabela = crawl.scrap_webpage_onet(url,'newsTitle')

        for x in tabela:
            desc = Onetpl(description=x)
            desc.save()

        string = ''
        for x in tabela:
            string += x + '|'

        kesz.set('onet-cache', string, 60)

        context = {'baza': tabela}
    else:
        onet_cache_array = onet_cache.split('|')
        context = {'baza': onet_cache_array}

    return render(request, 'onet.html', context)



def interia(request):

    context = None
    interia_cache = kesz.get('interia-cache')

    if interia_cache is None:
        Interiapl.objects.all().delete()
        url = "http://www.interia.pl"
        tabela = crawl.scrap_webpage_interia(url,'news-a')

        for x in tabela:
            desc = Interiapl(description=x)
            desc.save()

        string = ''
        for x in tabela:
            string += x + '|'

        kesz.set('interia-cache', string, 60)

        context = {'baza': tabela}
    else:
        interia_cache_array = interia_cache.split('|')
        context = {'baza': interia_cache_array}

    return render(request, 'interia.html', context)



def wp(request):
    Wppl.objects.all().delete()
    site_baza = crawl.scrap_webpage_wp('http://www.wp.pl')

    for x in site_baza:
        if x is not None:
            desc = Wppl(description=x)
            desc.save()

    context = {'baza': site_baza}
    return render(request, 'wp.html', context)




def wyszukane(request):
    zapytanie = request.session.get('lista')
    baza = Google.objects.all()

    print('ilosc artykulow: ' + str(len(baza)))
    title = ''
    if baza.count() == 0:
        title = 'brak artykulow'
    else:
        title = 'znalezione artykuly'
    context = {'baza' : baza , 'title' : title , 'zapytanie' : zapytanie}

    return render(request, 'wyszukane.html', context)



def pobieranieNewsow():
    news = []

    onet_news = Onetpl.objects.all()
    interia_news = Interiapl.objects.all()
    wp_news = Wppl.objects.all()

    if onet_news is not None:
        for x in onet_news:
            news.append(x.description)

    if interia_news is not None:
        for x in interia_news:
            news.append(x.description)

    if wp_news is not None:
        for x in wp_news:
            news.append(x.description)

    return news




def szukaj(request):
    form = ComForm(request.POST)
    if form.is_valid():

        case =0

        wyrazy = []

        Google.objects.all().delete()

        slowo = form.cleaned_data.get('slowo')

        lista_wynikow_do_cache = []

        print(kesz.get(str(slowo)))
        if kesz.get(str(slowo)) is not None:
            naglowki_z_cache = kesz.get(str(slowo))
            cache_array = naglowki_z_cache.split('|')
            for naglowek in cache_array:
                z = Google(description=naglowek)
                z.save()

            return redirect('wyszukane')


        slowaZplusem = slowo.split(' ')
        wyrazy = slowo.split()

        if ' ' in slowo:
            case = 1
        else:
            case = 0

        list = []
        list = pobieranieNewsow()

        for x in slowaZplusem:
            print(x)
            if str(x[0]) == '+':
                case = 2

        for x in slowaZplusem:
            print(x)
            if str(x[0]) == '-':
                case = 3

        if wyrazy[0][0] == '-':
            case = 3
        if wyrazy[0][0] == '"':
            case = 5


        g = 0

        if case == 1 or case == 0:
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                for j in kk:
                    for h in wyrazy:
                        if h.lower() == j.lower():
                            lista_wynikow_do_cache.append(k)
                            z = Google(description = k)
                            z.save()

        if case == 2:
            # te z plusem
            wymagane = []

            # te bez plusa
            wybrane_naglowki = []

            count = 0
            for s in slowaZplusem:
                print(s[0])
                if s[0] == '+':
                    slowo_do_dodania = s[1:]
                    print(slowo_do_dodania)
                    wymagane.append(slowo_do_dodania)
                    slowaZplusem.pop(count)
                count = count + 1

            for i in slowaZplusem:
                print(i)

            for slowo_nie_wymagane in slowaZplusem:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        print(naglowek)
                        wybrane_naglowki.append(naglowek)

            if len(slowaZplusem) == 0:
                wybrane_naglowki = list

            for slowo_wymagane in wymagane:
                print('# '+slowo_wymagane)
                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        print(naglowek)
                        lista_wynikow_do_cache.append(naglowek)
                        z = Google(description=naglowek)
                        z.save()
        if case == 3:
             # te z plusem
            wymagane = []

            # te bez plusa
            wybrane_naglowki = []

            count = 0
            for s in slowaZplusem:
                print(s[0])
                if s[0] == '-':
                    slowo_do_dodania = s[1:]
                    print(slowo_do_dodania)
                    wymagane.append(slowo_do_dodania)
                    slowaZplusem.pop(count)
                count = count + 1

            for i in slowaZplusem:
                print(i)

            for slowo_nie_wymagane in slowaZplusem:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        print(naglowek)
                        wybrane_naglowki.append(naglowek)


            if len(slowaZplusem) == 0:
                wybrane_naglowki = list

            for slowo_wymagane in wymagane:
                print('# '+slowo_wymagane)
                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        print(naglowek)
                    else:
                        lista_wynikow_do_cache.append(naglowek)
                        z = Google(description=naglowek)
                        z.save()

        if case == 5:
            length = len(slowo)
            wyraz_arr = slowo[1:length-1]
            wyraz = ''.join([str(x) for x in wyraz_arr])
            print(wyraz)
            print(str(len(list)))

            for i in list:
                if wyraz in i:
                    lista_wynikow_do_cache.append(i)
                    z = Google(description=i)
                    z.save()

        request.session['lista'] = slowo

        string = ''
        for i in lista_wynikow_do_cache:
            string += i + '|'

        kesz.set(slowo, string, 30)

        return redirect('wyszukane')

    return render(request,'wyszukaj.html', locals())
